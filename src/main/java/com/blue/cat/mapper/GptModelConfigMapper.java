package com.blue.cat.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.blue.cat.bean.entity.GptModelConfig;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lixin
 * @since 2023-08-01
 */
@Mapper
public interface GptModelConfigMapper extends BaseMapper<GptModelConfig> {

    @Select("select * from gpt_model_config where status = 1 ")
    List<GptModelConfig> getAllValidGptConfig();
}
